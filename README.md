# Forge World Time Macros 

This Forge app adds a collection of timezone related macros to your Confluence Cloud site.

### World Clock

Displays the current time in a specific location.

![World Clock](images/world-clock.png)

![World Clock Config](images/world-clock-config.png)

### World Time

Displays a specific time in a specific location.

![World Time](images/world-time.png)

![World Time Config](images/world-time-config.png)

## Installing this app
 
You will need Node.js and the Forge CLI to install this app. You can install the Forge CLI by running `npm install -g @atlassian/forge-cli`.

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

1. Clone this repository.
1. Run `forge register` to register a new copy of this app to your developer account.
1. Run `npm install` to install your dependencies.
1. Run `forge deploy` to deploy the app into the default environment.
1. Run `forge install` and follow the prompts to install the app into your Confluence site.
