import ForgeUI, { render, useConfig, Fragment, Text, TextField, Select, Option, ConfigForm, Macro } from "@forge/ui";

import moment from 'moment-timezone';
import { convertTime, formatTime, formats, getFlagForTimeZone } from './util';

const defaultConfig = {
  tz: 'America/Los_Angeles',  
  format: formats[0],
  inputTz: 'Australia/Sydney',
  time: moment().format('YYYY-MM-DD HH:mm')
};

const Config = () => {
  return (
    <ConfigForm>
      <Select label="Display Time Zone" name="tz">
        { moment.tz.names().map(element => {return (
          <Option label={element} value={element} />
        )})}
      </Select>
      <Select label="Display Format" name="format">
        { formats.map(format => {return (
          <Option 
            label={formatTime('America/Los_Angeles', format, null)} 
            value={format} />
        )})}
      </Select>  
      <Select label="Input Time Zone" name="inputTz">
        { moment.tz.names().map(element => {return (
          <Option label={element} value={element} />
        )})}
      </Select>
      <TextField 
        label="Input Time (YYYY-MM-DD HH:mm)"
        name="time" 
        defaultValue={ moment().format('YYYY-MM-DD HH:mm') } />
    </ConfigForm>
  );
}

const App = () => {
  const { inputTz, time, tz, format } = useConfig();

  const displayTime = convertTime(inputTz, time, tz, format);
  if (!displayTime) {
    return (
      <Fragment>
        <Text content='Error! **Input Time** must be in the format `YYYY-MM-DD HH:mm`' />
      </Fragment>
    )
  }

  const flag = getFlagForTimeZone(tz);
  return (
    <Fragment>
      <Text content={`${flag} ${displayTime}`} />
    </Fragment>
  );
};

export const run = render(
  <Macro 
    app={<App />}
    config={<Config />}
    defaultConfig={defaultConfig}
  />
);
