import moment from 'moment-timezone';
import ct from 'countries-and-timezones';
import ctToEmoji from 'country-code-to-flag-emoji';

export const formats = [
  'h:mma dddd, MMM Do (z)',
  'h:mma dddd (z)',
  'h:mma MMM Do (z)',
  'h:mma dddd, MMM Do',
  'h:mma dddd',
  'h:mma MMM Do'  
];

export function formatTime(tz, format, time) {
  const m = time ? moment(time) : moment();
  return m.tz(tz).format(format);
}

export function convertTime(inputTz, time, outputTz, format) {
  const convertedTime = moment
    .tz(time, inputTz)
    .clone()
    .tz(outputTz)
    .format(format);
  return convertedTime === 'Invalid date' ? null : convertedTime;
}

export function getFlagForTimeZone(tz) {
  const countryInfo = ct.getTimezone(tz);
  return countryInfo ? ctToEmoji(countryInfo.country) : '🏳';
}
